# Arduino firmware for svzrm app  

## Requirements:
  
- Arduino UNO Wifi rev2
- [arduino-cli](https://github.com/arduino/arduino-cli)

## Development
```bash
cp src/secrets.h.exmaple src/secrets.h # and update secrets
make install # install core dependencies
make deps # install software dependencies
make flush # compiles and upload program to Arduino
```
