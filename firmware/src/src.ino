#include "secrets.h"
#include "display.h"
#include "httpConnection.h"

unsigned long cms = 0;

Display display;
HttpConnection httpConnection;

void setup()
{
    Serial.begin(9600);
    display.init();
    display.clear();
    httpConnection.setup(WIFI_SSID, WIFI_PASS, BACKEND_HOST, BACKEND_PORT);
}

void loop()
{
    cms = millis();

    // example of display usage
    // display (line, text); // line 0-3, text max 20 chars
    display.render(1, "Cauky :)");
    display.render(3, "Vole :)");

    // example of backend call with rfid chip
    String name = httpConnection.callPersonArrived("123");
    if (httpConnection.success())
    {
        display.render(2, name);
    }
    else
    {
        display.render(2, String(httpConnection.getErrorCode()));
        display.render(2, httpConnection.getErrorMessage());
    }

    display.render(0, String(cms));
    delay(10000);
}