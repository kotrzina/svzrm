#ifndef http_connection_h
#define http_connection_h

#include <WiFiNINA.h>
#include <ArduinoHttpClient.h>

class HttpConnection
{
public:
    HttpConnection();
    void setup(char *ssid, char *pass, char *backendHost, int backendPort);
    void connect();

    String callPersonArrived(String rfid);

    bool success();
    String getErrorMessage();
    int getErrorCode();

private:
    struct moduleSettings
    {
        char *ssid;
        char *pass;
        char *backendHost;
        int backendPort;
    } settings;

    int status = WL_IDLE_STATUS; // wifi status
    WiFiSSLClient *sslClient;
    WiFiClient *client;
    HttpClient *backend;

    String lastErrorMessage = "";
    int lastErrorCode = 0;
};

#endif // http_connection_h