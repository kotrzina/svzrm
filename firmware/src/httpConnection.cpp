#include "httpConnection.h"

HttpConnection::HttpConnection()
{
}

void HttpConnection::setup(char *ssid, char *pass, char *backendHost, int backendPort)
{
    settings.ssid = ssid;
    settings.pass = pass;
    settings.backendHost = backendHost;
    settings.backendPort = backendPort;

    if (WiFi.status() == WL_NO_MODULE)
    {
        // @todo don't continue
    }

    String fv = WiFi.firmwareVersion();
    if (fv < WIFI_FIRMWARE_LATEST_VERSION)
    {
        // @todo upgrade the firmware
    }

    connect();
    client = new WiFiClient();
    sslClient = new WiFiSSLClient();
    backend = new HttpClient(*client, settings.backendHost, settings.backendPort);
}

void HttpConnection::connect()

{
    Serial.println("connecting...");
    status = WiFi.status();
    if (status != WL_CONNECTED)
    {
        while (status != WL_CONNECTED)
        {
            status = WiFi.begin(settings.ssid, settings.pass);
            delay(5000);
        }
    }
}

String HttpConnection::callPersonArrived(String rfid)
{
    lastErrorMessage = "";
    lastErrorCode = 0;

    backend->setTimeout(5000);
    backend->get("/api/person/rfid/" + rfid);

    int statusCode = backend->responseStatusCode();
    String response = backend->responseBody();
    String name = "";
    if (statusCode == 200)
    {
        for (int i = 0; i < response.length(); i++)
        {
            if (String(response[i]) == String("|"))
            {
                return name;
            }
            else
            {
                name += response[i];
            }
        }

        return name;
    }
    if (statusCode == 404)
    {
        lastErrorCode = 404;
        lastErrorMessage = "RFID tag " + rfid + " does not exist";
        return "";
    }

    lastErrorCode = statusCode;
    lastErrorMessage = "Unknown error";
    return "";
}

bool HttpConnection::success()
{
    return lastErrorCode == 0;
}

String HttpConnection::getErrorMessage()
{
    return lastErrorMessage;
}

int HttpConnection::getErrorCode()
{
    return lastErrorCode;
}

// void POST_example(String msg)
// {
//     String contentType = "application/json";
//     String postData = "{\"content\":\"" + msg + "\"}";

//     discord.post(
//         DISCORD_HOOK_PATH,
//         contentType,
//         postData);

//     int statusCode = discord.responseStatusCode();
//     String response = discord.responseBody();

//     if (statusCode != 204)
//     {
//         error(0x02);
//     }
// }