#include "display.h"

Display::Display()
{
    lcd = new LiquidCrystal_I2C(0x27, COLS, ROWS);
}

void Display::init()
{
    lcd->init();
    lcd->backlight();
    delayMicroseconds(100);
}

void Display::render(int line, String text)
{
    for (int i = text.length(); i < COLS; i++)
    {
        text = text + " ";
    }

    lcd->setCursor(0, line);
    lcd->print(text);
    lines[line] = text;
}

void Display::flash(String line1, String line2, int delay_ms)
{
    lcd->clear();
    lcd->setCursor(0, 1);
    lcd->print(line1);
    lcd->setCursor(0, 2);
    lcd->print(line2);
    delay(delay_ms);
    lcd->clear();
    for (int i = 0; i < ROWS; i++)
    {
        lcd->setCursor(0, i);
        lcd->print(lines[i]);
    }
}

void Display::clear()
{
    lcd->clear();
}

Display::~Display()
{
}