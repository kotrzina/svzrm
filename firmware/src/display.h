#ifndef Display_h
#define Display_h

#include "Arduino.h"
#include "Wire.h"
#include <LiquidCrystal_I2C.h>

#define COLS 0x14
#define ROWS 0x04

class Display
{
private:
    LiquidCrystal_I2C *lcd;
    String lines[ROWS];

public:
    Display();
    void init();
    void render(int line, String text);
    void flash(String line1, String line2, int delay);
    void clear();
    ~Display();
};

#endif