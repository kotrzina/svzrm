package people

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/go-redis/redis/v8"
	"github.com/nu7hatch/gouuid"
)

type Person struct {
	Uuid string `json:"uuid"`
	Name string `json:"name"`
	Rfid string `json:"rfid"`
}

type Store struct {
	redis        *redis.Client
	redisListKey string
	ctx          context.Context
}

func New(redis *redis.Client, prefix string, ctx context.Context) *Store {
	key := prefix + "-" + "people-list"
	return &Store{
		redis:        redis,
		redisListKey: key,
		ctx:          ctx,
	}
}

func (p *Store) FindByRfid(rfid string) (*Person, error) {

	people, err := p.Get()
	if err != nil {
		return nil, err
	}

	for _, person := range people {
		if person.Rfid == rfid {
			return &person, nil
		}
	}

	return nil, fmt.Errorf("could not find person with rfid tag: %s", rfid)
}

func (p *Store) Add(name, rfid string) error {
	currentPeople, err := p.Get()
	if err != nil {
		return err
	}
	for _, person := range currentPeople{
		if person.Rfid == rfid {
			return fmt.Errorf("rfid %s already registered", rfid)
		}
	}

	id, err := uuid.NewV4()
	if err != nil {
		return err
	}

	person := Person{
		Uuid: id.String(),
		Name: name,
		Rfid: rfid,
	}

	d, err := json.Marshal(person)
	if err != nil {
		return err
	}

	p.redis.RPush(p.ctx, p.redisListKey, d)

	return nil
}

func (p *Store) Get() ([]Person, error) {

	values := p.redis.LRange(p.ctx, p.redisListKey, 0, -1).Val()

	var people []Person

	for _, person := range values {
		var personStruct Person
		if err := json.Unmarshal([]byte(person), &personStruct); err != nil {
			return []Person{}, err
		}

		people = append(people, personStruct)
	}

	return people, nil
}
