package server

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/kotrzina/svzrm/backend/pkg/attendance"
	"gitlab.com/kotrzina/svzrm/backend/pkg/consumption"
	"gitlab.com/kotrzina/svzrm/backend/pkg/people"
	"gitlab.com/kotrzina/svzrm/backend/pkg/tildeson"
	"net/http"
	"strconv"
)

type Application struct {
	people      *people.Store
	consumption *consumption.Store
	attendance  *attendance.Store
}

func New(p *people.Store, c *consumption.Store, a *attendance.Store) *Application {
	return &Application{
		people:      p,
		consumption: c,
		attendance:  a,
	}
}

func (a *Application) CreatePerson(c *gin.Context) {
	type requestShape struct {
		Name string `json:"name"`
		Rfid string `json:"rfid"`
	}

	var request requestShape
	if err := c.ShouldBindJSON(&request); err != nil {
		createErrorResponse(c, http.StatusBadRequest, fmt.Sprintf("invalid input data: %s", err.Error()))
		return
	}

	if request.Name == "" || request.Rfid == "" {
		createErrorResponse(c, http.StatusBadRequest, "missing values in request data")
		return
	}

	if err := a.people.Add(request.Name, request.Rfid); err != nil {
		createErrorResponse(c, http.StatusBadRequest, fmt.Sprintf("invalid input data: %s", err.Error()))
		return
	}

	c.Render(http.StatusNoContent, tildeson.Tildeson{})
}

func (a *Application) GetPeople(c *gin.Context) {
	p, err := a.people.Get()
	if err != nil {
		createErrorResponse(c, http.StatusInternalServerError, "could not retrieve data")
		return
	}

	var t tildeson.Tildeson
	for _, i := range p {
		t = append(t, i.Name)
		t = append(t, i.Rfid)
		t = append(t, i.Uuid)
	}

	c.Render(http.StatusOK, t)
}

func (a *Application) GetPersonByRfid(c *gin.Context) {
	rfid := c.Param("rfid")
	if rfid == "" {
		createErrorResponse(c, http.StatusBadRequest, "could not parse rfid from url")
		return
	}

	p, err := a.people.FindByRfid(rfid)
	if err != nil {
		createErrorResponse(c, http.StatusNotFound, "person does not exist")
		return
	}

	c.Render(http.StatusOK, tildeson.Tildeson{p.Name, p.Rfid, p.Uuid})
}

func (a *Application) Register(c *gin.Context) {
	type requestShape struct {
		Rfid string `json:"rfid"`
	}

	var request requestShape
	if err := c.ShouldBindJSON(&request); err != nil {
		createErrorResponse(c, http.StatusBadRequest, fmt.Sprintf("invalid input data: %s", err.Error()))
		return
	}

	if request.Rfid == "" {
		createErrorResponse(c, http.StatusBadRequest, "missing values in request data: rfid")
		return
	}

	if err := a.attendance.Register(request.Rfid); err != nil {
		createErrorResponse(c, http.StatusBadRequest, fmt.Sprintf("could not register rfid: %s", err.Error()))
		return
	}

	c.Render(http.StatusNoContent, tildeson.Tildeson{})
}

func (a *Application) CreateConsumptionRecord(c *gin.Context) {
	type requestShape struct {
		Weight string `json:"weight"`
	}

	var request requestShape
	if err := c.ShouldBindJSON(&request); err != nil {
		createErrorResponse(c, http.StatusBadRequest, fmt.Sprintf("invalid input data: %s", err.Error()))
		return
	}

	// parse weight from request
	weight64, err := strconv.ParseFloat(request.Weight, 32)
	if err != nil {
		createErrorResponse(c, http.StatusBadRequest, "could not parse weight")
		return
	}
	weight := float32(weight64)

	a.consumption.Add(weight)

	c.Render(http.StatusNoContent, tildeson.Tildeson{})
}

func createErrorResponse(c *gin.Context, code int, msg string) {
	c.Render(code, tildeson.Tildeson{"err", msg})

	c.Abort()
}
