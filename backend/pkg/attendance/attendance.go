package attendance

import (
	"context"
	"fmt"
	"github.com/go-redis/redis/v8"
	"gitlab.com/kotrzina/svzrm/backend/pkg/people"
	"time"
)

type Store struct {
	people       *people.Store
	redis        *redis.Client
	redisListKey string
	ctx          context.Context
}

func New(people *people.Store, redis *redis.Client, prefix string, ctx context.Context) *Store {
	return &Store{
		people:       people,
		redis:        redis,
		redisListKey: prefix + "-attendance",
		ctx:          ctx,
	}
}

func (s *Store) Register(rfid string) error {
	person, err := s.people.FindByRfid(rfid)
	if err != nil {
		return fmt.Errorf("person is not registered. rfid: %w", err)
	}

	s.redis.LPush(s.ctx, s.redisListKey, person.Uuid)
	s.redis.Expire(s.ctx, s.redisListKey, getAttendanceExpirationDuration())

	return nil
}

func (s *Store) GetList() ([]people.Person, error) {
	var now []people.Person

	peopleList, err := s.people.Get()
	if err != nil {
		return now, err
	}

	registeredList := s.redis.LRange(s.ctx, s.redisListKey, 0, -1).Val()

	for _, p := range peopleList {
		for _, r := range registeredList {
			if r == p.Uuid {
				now = append(now, p)
			}
		}
	}

	return now, nil
}

func getAttendanceExpirationDuration() time.Duration {
	now := time.Now()
	hours := 24 - now.Hour() - 1     // remaining hours
	seconds := 60 - now.Second() - 1 // remaining seconds
	diff := hours*60 + seconds       // seconds until midnight

	return time.Duration(diff) * time.Second
}
