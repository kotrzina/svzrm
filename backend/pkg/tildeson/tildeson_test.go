package tildeson

import (
	"strings"
	"testing"
)

type testCase struct {
	data     []string
	expected string
}

func TestTildeson(t *testing.T) {
	cases := []testCase{
		{
			data:     []string{"foo", "bar"},
			expected: "foo|bar",
		},
		{
			data:     []string{},
			expected: "",
		},
	}

	for _, test := range cases {
		rendered, err := Tildeson(test.data).String()
		if err != nil {
			t.Errorf("tildeson error not expected in %s", strings.Join(test.data, ","))
		}

		if rendered != test.expected {
			t.Errorf("tildeson error should be equal %s == %s", rendered, test.expected)
		}
	}
}

func TestTildesonError(t *testing.T) {
	cases := []testCase{
		{
			data:     []string{"foo", "b|ar"},
			expected: "foo|bar",
		},
		{
			data:     []string{"|"},
			expected: "",
		},
	}

	for _, test := range cases {
		_, err := Tildeson(test.data).String()
		if err == nil {
			t.Errorf("tildeson render error expected in %s", strings.Join(test.data, ","))
		}
	}
}
