package tildeson

import (
	"fmt"
	"net/http"
	"strings"
)

type Tildeson []string

var plainContentType = []string{"text/plain; charset=utf-8"}

func (t Tildeson) String() (string, error) {
	for _, v := range t {
		if strings.Contains(v, "|") {
			return "", fmt.Errorf("one of the values contain |")
		}
	}
	return strings.Join(t, "|"), nil
}

// Render (String) writes data with custom ContentType.
func (t Tildeson) Render(w http.ResponseWriter) error {
	data, err := t.String()
	if err != nil {
		return err
	}

	_, err = w.Write([]byte(data))
	return err
}

// WriteContentType (String) writes Plain ContentType.
func (t Tildeson) WriteContentType(w http.ResponseWriter) {
	header := w.Header()
	if val := header["Content-Type"]; len(val) == 0 {
		header["Content-Type"] = plainContentType
	}
}
