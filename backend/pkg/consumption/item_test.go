package consumption

import "testing"

func TestItem(t *testing.T) {
	testingValue := float32(22.478)
	item := createItem(testingValue)
	encoded := item.Encode()
	decodedItem, err := Decode(encoded)
	if err != nil {
		t.Errorf(err.Error())
	}

	if decodedItem.Value != testingValue {
		t.Errorf("invalid encode and decode Item. Calculated: %f", decodedItem.Value)
	}
}
