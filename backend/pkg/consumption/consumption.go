package consumption

import (
	"context"
	"github.com/go-redis/redis/v8"
)

const pullItemCount = 500
const maxItemCount = int64(2000)

type Store struct {
	redis        *redis.Client
	redisListKey string
	ctx          context.Context
}

func New(redis *redis.Client, prefix string, ctx context.Context) *Store {
	return &Store{
		redis:        redis,
		redisListKey: prefix + "-consumption",
		ctx:          ctx,
	}
}

func (s *Store) Add(value float32) {
	item := createItem(value)
	data := item.Encode()
	s.redis.LPush(s.ctx, s.redisListKey, data)
	s.deleteUnusedValues()
}

func (s *Store) Get() ([]Item, error) {
	var data []Item
	values := s.redis.LRange(s.ctx, s.redisListKey, 0, pullItemCount-1).Val()

	for _, value := range values {
		i, err := Decode(value)
		if err != nil {
			return []Item{}, err
		}

		data = append(data, *i)
	}

	return data, nil
}

func (s *Store) deleteUnusedValues() {
	len := s.redis.LLen(s.ctx, s.redisListKey).Val()
	if len > maxItemCount {
		for i := maxItemCount; i < len; i++ {
			_ = s.redis.RPop(s.ctx, s.redisListKey)
		}
	}
}
