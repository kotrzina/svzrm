package consumption

import (
	"fmt"
	"strconv"
	"strings"
	"time"
)

type Item struct {
	Time  time.Time
	Value float32
}

func createItem(v float32) *Item {
	return &Item{
		Time:  time.Now(),
		Value: v,
	}
}

const itemSeparator = "|"

func (i *Item) Encode() string {
	return fmt.Sprintf("%s%s%f", i.Time.Format(time.RFC3339), itemSeparator, i.Value)
}

func Decode(decoded string) (*Item, error) {
	parts := strings.Split(decoded, itemSeparator)
	if len(parts) != 2 {
		return nil, fmt.Errorf("could not decode Item from list")
	}

	t, err := time.Parse(time.RFC3339, parts[0])
	if err != nil {
		return nil, fmt.Errorf("could not decode Time from Item, err: %w", err)
	}

	v, err := strconv.ParseFloat(parts[1], 32)
	if err != nil {
		return nil, fmt.Errorf("could not decode Value from Item, err: %w", err)
	}

	return &Item{
		Time:  t,
		Value: float32(v),
	}, nil
}
