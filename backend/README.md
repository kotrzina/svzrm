# Backend for svzrm app

## Configuration environment variables


| Name              | Example        | Description               | Default |
|-------------------|----------------|---------------------------|---------|
| REDIS_HOST        | localhost:6379 | port can be included      |         |
| REDIS_DB          | 0              | selected database         |         |
| REDIS_PREFIX      | xyz            | prefix for all redis keys |         |
| ENABLE_LIMITER    | 1              | Enable rate limits        | 0       |
| ENABLE_PROMETHEUS | 1              | Enable endpoint /metrics  | 0       |

