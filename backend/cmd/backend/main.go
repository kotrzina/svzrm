package main

import (
	"context"
	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis/v8"
	"github.com/joho/godotenv"
	"github.com/ulule/limiter/v3"
	mgin "github.com/ulule/limiter/v3/drivers/middleware/gin"
	"github.com/ulule/limiter/v3/drivers/store/memory"
	ginprometheus "github.com/zsais/go-gin-prometheus"
	"gitlab.com/kotrzina/svzrm/backend/pkg/attendance"
	"gitlab.com/kotrzina/svzrm/backend/pkg/consumption"
	"gitlab.com/kotrzina/svzrm/backend/pkg/people"
	"gitlab.com/kotrzina/svzrm/backend/pkg/server"
	"net/http"
	"os"
	"strconv"
	"strings"
)

type config map[string]string

func main() {
	_ = godotenv.Load(".env")

	cfg := createConfig()
	app := createApp(cfg)

	r := gin.Default()
	if cfg["ENABLE_LIMITER"] == "1" {
		useLimiter(r)
	}
	if cfg["ENABLE_PROMETHEUS"] == "1" {
		usePrometheus(r)
	}

	r.GET("/api/person", app.GetPeople)
	r.GET("/api/person/rfid/:rfid", app.GetPersonByRfid)
	r.POST("/api/person", app.CreatePerson)
	r.POST("/api/register", app.Register)
	r.POST("/api/consumption-record", app.CreateConsumptionRecord)

	r.GET("/ping", ping)

	r.Run(":8080")
}

func createConfig() map[string]string {
	keys := []string{
		"REDIS_HOST",
		"REDIS_DB",
		"REDIS_PREFIX",
		"ENABLE_LIMITER",
		"ENABLE_PROMETHEUS",
	}

	envs := make(config, len(keys))
	for _, key := range keys {
		envs[key] = os.Getenv(key)
	}

	return envs
}

func createApp(cfg config) *server.Application {
	idx, err := strconv.Atoi(cfg["REDIS_DB"])
	if err != nil {
		// panic("could not read redis database in env variable REDIS_DB")
	}

	db := redis.NewClient(&redis.Options{
		Addr:     cfg["REDIS_HOST"],
		Password: "",
		DB:       idx,
	})

	redisCtx := context.Background()
	err = db.Ping(redisCtx).Err()
	if err != nil {
		panic("could not connect to redis")
	}
	peo := people.New(db, cfg["REDIS_PREFIX"], redisCtx)
	con := consumption.New(db, cfg["REDIS_PREFIX"], redisCtx)
	att := attendance.New(peo, db, cfg["REDIS_PREFIX"], redisCtx)

	return server.New(peo, con, att)
}

func useLimiter(engine *gin.Engine) {
	// 1000 req/hour MAX
	// then 429 response code
	rate, err := limiter.NewRateFromFormatted("1000-H")
	if err != nil {
		panic(err)
	}

	store := memory.NewStore()
	l := limiter.New(store, rate)

	middleware := mgin.NewMiddleware(l)
	engine.Use(middleware)
}

func usePrometheus(engine *gin.Engine) {
	p := ginprometheus.NewPrometheus("gin")
	p.ReqCntURLLabelMappingFn = func(c *gin.Context) string {
		url := c.Request.URL.Path
		for _, p := range c.Params {
			if p.Key == "name" {
				url = strings.Replace(url, p.Value, ":name", 1)
				break
			}
		}
		return url
	}
	p.Use(engine)
}

func ping(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"message": "pong",
	})
}
