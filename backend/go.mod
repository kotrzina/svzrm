module gitlab.com/kotrzina/svzrm/backend

go 1.13

require (
	github.com/gin-gonic/gin v1.7.4
	github.com/go-redis/redis/v8 v8.4.2
	github.com/joho/godotenv v1.3.0
	github.com/nu7hatch/gouuid v0.0.0-20131221200532-179d4d0c4d8d
	github.com/prometheus/client_golang v1.11.0 // indirect
	github.com/sirupsen/logrus v1.8.1 // indirect
	github.com/ulule/limiter/v3 v3.8.0
	github.com/zsais/go-gin-prometheus v0.1.0
)
